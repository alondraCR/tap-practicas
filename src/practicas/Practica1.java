/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Alondra
 */
public class Practica1 extends JFrame implements ActionListener{

    JButton b1;
    JLabel label;
    JTextField nombre;
    
    Practica1 (){
        this.setTitle("Practica 1");
        this.setSize(300,150);
        this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        b1 = new JButton("¡Saludar!");
        label = new JLabel("Escribe un nombre para saludar: ");
        nombre = new JTextField(20);
        
        this.add(label);
        this.add(nombre);
        this.add(b1);
        
        b1.addActionListener(this);
    }
    
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
                new Practica1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "Hola "+ nombre.getText());
    }
    
}
