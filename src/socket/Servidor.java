/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alumno
 */
public class Servidor {
    public static void main(String[] args) {
        ServerSocket server = null;
        Socket conn;
        
        System.out.println("Iniciando proceso de servidor...");
        
        try{
            server = new ServerSocket(1001);
            
        }catch(Exception e){
        
        }
            while(server!=null){
                try{
                    conn = server.accept();                            
                    System.out.printf("Se acepta conexion desde %n \n",conn.getInetAddress());
                
                    PrintWriter out = 
                            new PrintWriter(conn.getOutputStream(), true);
                    BufferedReader in =
                            new BufferedReader(
                                    new InputStreamReader(conn.getInputStream()));
                    
                    BufferedReader stdIn =
                        new BufferedReader(
                            new InputStreamReader(System.in));
                        
                    String saludo = in.readLine();
            
                    System.out.printf("Se recibe cadena de %s, con el contenido de: %s \n",conn.getInetAddress(),saludo);
            
                    String respuesta = stdIn.readLine();
                    conn.close();
                    
                }catch(IOException ex){
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
        
}

